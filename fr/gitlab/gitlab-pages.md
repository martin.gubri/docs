# Comment utiliser les Gitlab Pages

Les Gitlab Pages permettent à qui possède un compte sur <https://framagit.org> (qui utilise [Gitlab][1]) de publier un site statique sur notre infrastructure, soit en sous-domaine de frama.io (par exemple : <https://luc.frama.io>) soit avec un domaine rien qu'à vous.

Les Gitlab Pages remplacent (avantageusement) [FsPages][2] que nous proposions auparavant.

Tout d'abord, voici le lien de la documentation officielle des Gitlab Pages : <https://docs.gitlab.com/ce/user/project/pages/index.html>.

## Comment ça marche ?

Les Gitlab Pages utilisent Gitlab CI, le système d'intégration continue de Gitlab, pour générer des sites statiques à l'aide de générateurs tels [Hugo][3] ou [Pelican][4] (vous pouvez en utiliser bien d'autres ou aucun).

À chaque *push*, Gitlab CI va démarrer un conteneur docker (que vous aurez préalablement choisi), exécuter un script (que vous aurez écrit) et récupérera un dossier `public` pour le publier.

## Avantages par rapport à FsPages

Premièrement, vous n'avez nul besoin d'installer un générateur de site statique sur votre ordinateur pour publier via les Gitlab Pages, contrairement à FsPages (à moins que vous ne fassiez tout à la main). À noter cependant que la plupart des générateurs possèdent une commande vous permettant de créer facilement l'arborescence idoine. Mais une fois le système mis en place, vous n'aurez plus besoin du générateur 🙂

Vous n'aurez même plus besoin de rien puisque vous pourrez modifier directement vos fichiers sources dans l'interface web de Gitlab et laisser Gitlab Pages regénérer votre site (mais bon, ça, ça nécessite que vous ayez déjà mis en place tout le système).

Deuxièmement, vous pouvez utiliser le nom de domaine que vous voulez ! Vous n'êtes plus obligés de vous contenter de https://votre_user.frama.io. Et vous pourrez aussi déposer un certificat afin que votre site avec votre nom de domaine personnel soit joignable en HTTPS.

## Exemple d'un site généré avec Hugo

### Prise en main d'Hugo

Pour plus de détails, rendez-vous sur <https://gohugo.io/overview/quickstart/>

*   installez Hugo sur votre ordinateur : rendez-vous sur <https://github.com/spf13/hugo/releases>, téléchargez l'installeur correspondant à votre système d'exploitation et lancez-le
*   si vous avez déjà un dépôt git, rendez-vous dans son dossier, sinon créez-en un sur <https://framagit.org/projects/new>, clonez-le et allez dans son dossier
*   créez un nouveau site avec Hugo : `hugo new site mon_site; cd mon_site`
*   éditez le fichier `config.toml`
*   ajoutez un [thème][5] dans le dossier `themes`
*   créez du contenu pour votre site : `hugo new post/toto.md`
*   éditez `content/post/toto.md`
*   regardez ce que ça donne en lançant `hugo server --theme=hugo_theme_robust --buildDrafts` puis en vous rendant sur <http://localhost:1313/>
*   si vous êtes satisfait du résultat, éditez `content/post/toto.md` et passez `draft à false`
*   pour éviter de devoir passer l'argument `--theme=hugo_theme_robust` à hugo à chaque fois, ajoutez `theme = "hugo_theme_robust"` à votre fichier `config.toml`

### Publication via les Gitlab Pages

Pour plus de détails, rendez-vous sur <https://gohugo.io/tutorials/hosting-on-gitlab/>

```
echo "/public/" >> .gitignore
vi .gitlab-ci.yml
```

*Concernant l'intégration continue* :

* si vous n'avez pas encore de fichier `.gitlab-ci.yml`, vous pouvez choisir un modèle en vous rendant dans `Set up CI` et en choisissant le template `Hugo`,
* ou bien ajoutez ceci à votre fichier `.gitlab-ci.yml` (attention ! Si vous avez déjà de l'intégration continue en place, il peut être opportun soit d'utiliser votre image classique et d'y installer hugo (comme sur, par exemple, <https://gitlab.com/pages/hugo>) soit de faire une branche dédiée).

```
image: publysher/hugo

pages:
  script:
    - hugo
  artifacts:
    paths:
      - public
  only:
    - master
```

Puis :

```
git add --all
git commit -m "Publication de mon super site"
git push
```

Et voilà ! Votre site devrait être accessible à l'adresse https://votre_user.frama.io/votre_depot ou directement à https://votre_user.frama.io/ si votre dépôt s'appelle votre_user.frama.io.

Rendez-vous sur <https://framagit.org/votre_user/votre_depot/pages> si vous souhaitez que votre site soit accessible avec le nom de domaine de votre choix.

### Passer de FsPages aux Gitlab Pages

Si vous utilisiez Fspages, il est très simple d'adapter votre dépôt git pour qu'il utilise désormais Gitlab Pages. En effet, FsPages ne permettait que de publier des dossiers contenant des pages HTML déjà préparées, et les Gitlab Pages permettent de faire la même chose.

*   supprimez le webhook qui servait pour FsPages (sur <https://framagit.org/votre_user/votre_projet/settings/integrations>), il s'agit du webhook à l'adresse semblable à http://127.0.0.1:4246
*   copiez le fichier .gitlab-ci.yml de <https://gitlab.com/pages/plain-html> dans votre dépôt git
*   modifiez ce fichier :
    *   si le dossier à publier ne s'appelle pas `public`, à la place de `echo 'Nothing to do...'`, mettez `mv votre_dossier public`
    *   au lieu de `master` tout en bas, indiquez `fs-pages` (sauf si vous voulez vous débarrasser de la branche fs-pages, auquel cas il vous faut copier le contenu de la branche fs-pages dans la branche master)
*   supprimez le fichier `.fs-pages.json`
*   commitez et pushez 🙂

Si vous souhaitez que votre dépôt git fournisse la racine de votre site (i.e. qu'il ne soit pas publié dans un sous-dossier de https://votre_user.frama.io), il suffit de renommer votre dépôt en votre_user.frama.io (exemple : <https://framagit.org/luc/luc.frama.io>) via la page https://framagit.org/votre_user/votre_projet/edit

## Rediriger vers la version HTTPS

Malheureusement, contrairement à notre installation de FsPages, les sites ne sont pas redirigés vers la version HTTPS s'il en existe une (pas même pour les sous-domaines de frama.io, qui bénéficient automatiquement d'une version HTTPS). Un [ticket][6] est cependant ouvert chez Gitlab à ce sujet.

En attendant, pour pallier ce problème, vous pouvez inclure ce petit bout de javascript dans vos pages HTML :

```
<script>
    var loc = window.location.href+'';
    if (loc.indexOf('http://')==0){
        window.location.href = loc.replace('http://','https://');
    }
</script>
```

## Utiliser Let's Encrypt

L'utilisation de certificats [Let's Encrypt][7] n'est pas [aisée][8] mais un [ticket][9] est ouvert chez Gitlab pour intégrer directement Let's Encrypt aux Gitlab Pages.

 [1]: https://framacloud.org/cultiver-son-jardin/installation-de-gitlab-et-mattermost/
 [2]: https://framagit.org/luc/fs-pages
 [3]: https://gohugo.io/
 [4]: https://blog.getpelican.com/
 [5]: https://themes.gohugo.io/
 [6]: https://gitlab.com/gitlab-org/gitlab-ce/issues/28857
 [7]: https://letsencrypt.org/
 [8]: https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/
 [9]: https://gitlab.com/gitlab-org/gitlab-ce/issues/28996

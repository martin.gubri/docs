# Quelques mots d'introduction sur Git

### 1. Description

**Git** est un logiciel de gestion de versions décentralisé. C'est un logiciel libre créé en 2005 par [Linus Torvalds](https://fr.wikipedia.org/wiki/Linus_Torvalds) lui-même, et distribué selon les termes de la [licence publique générale GNU](https://fr.wikipedia.org/wiki/Licence_publique_générale_GNU) version 2. En 2016, il s’agit du logiciel de gestion de versions le plus populaire,  utilisé par plusieurs millions de personnes à travers le monde.

Plus d'information concernant **Git** sur [Wikipédia](https://fr.wikipedia.org/wiki/Git).

### 2. Fonctionnalité

Git est un outil de bas niveau, simple et performant, qui a pour but principal de gérer l'évolution d'une arborescence de fichiers.

Dès sa conception Git a été pensé pour fonctionner de manière décentralisée, et c'est l'une des clefs de son succès. Il a beaucoup apporté au logiciel libre car il suffit de _cloner_ un projet pour commencer à travailler dessus, puis de proposer ses modifications/améliorations (que l'on appellera plus loin _pull requests_) au dépôt principal.

### 3. Framagit

Le site [Framasoft](https://framasoft.org/) (dont la devise, rappelons le, est : *la route est longue mais la voie est libre*) propose maintenant, entre autres services orientés logiciel libre, une forge logicielle Framagit, qui est une [évolution](https://framablog.org/2016/04/19/notre-gitlab-evolue-en-framagit-cest-tres-efficace/) de Gitlab.

Le site [Framagit](https://framagit.org/public/projects) héberge donc un certain nombre de projets, qui vont utiliser la méthodologie et les fonctionnalités du logiciel Git, pour leurs développements collaboratifs.


<p class="alert alert-info">
Remarque : Ce tutoriel s’appuie sur des exemples pratiques pris sur le dépôt « <i>Communication<i> » du collectif Emmabuntüs, car ce tutoriel a été fait initialement pour un usage interne de notre collectif, et nous avons ensuite proposé celui-ci pour la documentation de Framasoft.
</p>


----

## Faisons le point

Maintenant que les présentations sur le but des ce logiciel sont faites nous pouvons rentrer dans le vif du sujet en commençant par [la création et la configuration de votre compte sur Framagit](2-creation-configuration-compte.html).

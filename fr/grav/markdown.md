# La syntaxe markdown

Pour formater votre texte vous avez la possibilité d'utiliser la barre d'outils située au-dessus de la zone de texte, ou vous pouvez utiliser la syntaxe markdown.

## Styles de texte

Vous pouvez utiliser `_` ou `*` autour d'un mot pour le mettre en italique. Mettez-en deux pour le mettre en gras.
*  `_italique_` s'affiche ainsi : _italique_  
*  `**gras**` s'affiche ainsi : **gras**  
* `**_gras-italique_**` s'affiche ainsi : **_gras-italique_**  
* `~~barré~~` s'affiche ainsi : ~~barré~~


## Blocs de code

Créez un bloc de code en indentant chaque ligne avec quatre espaces, ou en mettant trois accents graves sur la ligne au dessus et en dessous de votre code.  
Exemple :  

` ```bloc de code``` `


s'affiche ainsi :

```
bloc de code
```

## Liens

Créez un lien intégré en mettant le texte désiré entre crochets et le lien associé entre parenthèses.

`Aidez-vous avec [la documentation de Framasite](https://docs.framasoft.org/fr/grav/) !`

s'affichera :

Aidez-vous avec [la documentation de Framasite](https://docs.framasoft.org/fr/grav/) !

## Images

Utilisez une image en ligne en copiant son adresse (finissant par `.jpg`, `.png`, `.gif` etc…) avec un texte alternatif entre crochets (qui sera affiché si l'image n'apparaît pas) et le lien entre parenthèses. Vous pouvez aussi ajouter un texte qui apparaîtra au survol de la souris grâce aux `"`.

```
![le logo de Framasoft](https://framasoft.org/nav/img/logo.png)
```
donnera :

![le logo de Framasoft](https://framasoft.org/nav/img/logo.png)

On peut ajouter un texte au survol :

```
![Le logo de Framasoft](https://framasoft.org/nav/img/logo.png "Un bien beau logo !")
```
Qui donnera (survolez l'image avec votre souris pour voir apparaître le texte) :

![Le logo de Framasoft](https://framasoft.org/nav/img/logo.png "Un bien beau logo !")

## Citation


Les citations se font avec le signe `>` :

```
> Oh la belle prise !
```

> Oh la belle prise !

## Listes

Vous pouvez créer des listes avec les caractères `*` et `-` pour des listes non ordonnées ou avec des nombres pour des listes ordonnées.

Une liste non ordonnée :

```
* une élément
* un autre
 * un sous élément
 * un autre sous élément
* un dernier élément
```

* une élément
* un autre
 * un sous élément
 * un autre sous élément
* un dernier élément

Une liste ordonnée :

```
1. élément un
2. élément deux
```

1. élément un
2. élément deux

## Titres

Pour faire un titre, vous devez mettre un `#` devant la ligne. Pour faire un titre plus petit, ajoutez un `#` (jusque 6) :

```
# Un grand titre
## Un titre un peu moins grand
### Un titre encore moins grand
```
Vous pouvez également souligner le texte en utilisant `===` ou `---` pour créer des titres.

```
Un grand titre
=============
```

## Tableaux

Pour créer un tableau vous devez placer une ligne de tirets (`-`) sous la ligne d'entête et séparer les colonnes avec des `|`. Vous pouvez aussi préciser l'alignement en utilisant des `:`. :

```
| Aligné à gauche  | Centré          | Aligné à droite |
| :--------------- |:---------------:| -----:|
| Aligné à gauche  |   ce texte        |  Aligné à droite |
| Aligné à gauche  | est             |   Aligné à droite |
| Aligné à gauche  | centré          |    Aligné à droite |
```

| Aligné à gauche  | Centré          | Aligné à droite |
| :--------------- |:---------------:| -----:|
| Aligné à gauche  |   ce texte        |  Aligné à droite |
| Aligné à gauche  | est             |   Aligné à droite |
| Aligné à gauche  | centré          |    Aligné à droite |

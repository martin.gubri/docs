# Composants de base

## Boutons

<p>
<button type="button" class="btn btn-default ">Default</button>
<button type="button" class="btn btn-primary ">Primary</button>
<button type="button" class="btn btn-success ">Success</button>
<button type="button" class="btn btn-info ">Info</button>
<button type="button" class="btn btn-warning ">Warning</button>
<button type="button" class="btn btn-danger ">Danger</button>
<button type="button" class="btn btn-link ">Link</button>
</p>

``` markdown
[g-button button_type="default" button_label="Default"][/g-button]
[g-button button_type="primary" button_label="Primary"][/g-button]
[g-button button_type="success" button_label="Success"][/g-button]
[g-button button_type="info" button_label="Info"][/g-button]
[g-button button_type="warning" button_label="Warning"][/g-button]
[g-button button_url="https://frama.site" button_type="danger" button_label="Danger"][/g-button]
[g-button button_type="link" button_label="Link"][/g-button]
```

### Bouton cliquable

<p><a href="https://frama.site" class="btn btn-default">Frama.site</a></p>

``` markdown
[g-button button_url="https://frama.site" button_type="default" button_label="Frama.site"][/g-button]
```

___

## Icônes

Par défaut le raccourci `g-icon` utilse les « Glyphicon » de **[Bootstrap](https://getbootstrap.com/docs/3.3/components/#glyphicons)** :

<p><span class="glyphicon glyphicon-envelope"></span></p>

``` markdown
[g-icon icon=envelope][/g-icon]
```

Vous pouvez utiliser des icônes **[Font Awesome](http://fortawesome.github.io/Font-Awesome/icons/)** en ajoutant simplement l’attribut `icon_type="fontawesome"`.
<p>
<i class="fa fa-firefox "></i>
<i class="fa fa-firefox fa-2x"></i>
<i class="fa fa-firefox fa-3x"></i>
<i class="fa fa-firefox fa-4x"></i>
</p>

``` markdown
[g-icon icon=firefox icon_type="fontawesome"][/g-icon]
[g-icon icon=firefox icon_type="fontawesome" icon_attributes="class:fa-2x"][/g-icon]
[g-icon icon=firefox icon_type="fontawesome" icon_attributes="class:fa-3x"][/g-icon]
[g-icon icon=firefox icon_type="fontawesome" icon_attributes="class:fa-4x"][/g-icon]
```

### Icônes multiples

<p><span class="fa-stack">
  <i class="fa fa-stack-2x fa-circle-thin"></i>
  <i class="fa fa-stack-1x fa-linux"></i>
</span>

<span class="fa-stack fa-lg">
  <i class="fa fa-stack-2x fa-circle-thin"></i>
  <i class="fa fa-stack-1x fa-linux"></i>
</span>

<br>
<span class="fa-stack">
  <i class="fa fa-stack-2x fa-square-o"></i>
  <i class="fa fa-stack-1x fa-linux"></i>
</span>

<span class="fa-stack fa-lg">
  <i class="fa fa-stack-2x fa-square-o"></i>
  <i class="fa fa-stack-1x fa-linux"></i>
</span>
</p>

``` markdown
[g-icon-stacked icon=linux][/g-icon-stacked]
[g-icon-stacked icon=linux large_icon=true][/g-icon-stacked]
[g-icon-stacked icon=linux icon_container="square-o"][/g-icon-stacked]
[g-icon-stacked icon=linux icon_container="square-o" large_icon=true][/g-icon-stacked]
```

### Icônes et liens

<p><a href="https://frama.site">Lien simple</a><br>

<a href="https://frama.site"><span class="glyphicon glyphicon-envelope "></span>
Lien Glyphicon</a><br>

<a href="https://frama.site"><i class="fa fa-firefox "></i>
Lien Fontawesome</a><br>

<a href="https://frama.site"><span class="fa-stack">
  <i class="fa fa-stack-2x fa-square-o"></i>
  <i class="fa fa-stack-1x fa-firefox"></i>
</span>
Lien Fontawesome avec icônes multiples</a>
</p>

``` markdown
[g-link url="https://frama.site" menu="Lien simple"][/g-link]
[g-link url="https://frama.site" menu="Lien Glyphicon" icon=envelope][/g-link]
[g-link url="https://frama.site" menu="Lien Fontawesome" icon_type="fontawesome" icon=firefox][/g-link]
[g-link url="https://frama.site" menu="Lien Fontawesome avec icônes multiples" icon_type="fontawesome" icon=firefox icon_container="square-o" stacked=true][/g-link]
```
___

## Listes

Les listes peuvent être créées en utilisant la syntaxe markdown habituelle 
mais pour les personnaliser il faudra utiliser une classe CSS et un code
de ce genre :

### Liste à puces

``` markdown
[g-list attributes="class:ma-classe"]
[g-list-item attributes="class:li-class"]Élément 1[/g-list-item]
[g-list-item]Élément 2[/g-list-item]
[g-list-item]Élément 3[/g-list-item]
[/g-list]
```

### Liste numérotée

``` markdown
[g-list attributes="class:ma-classe"  tag="ol"]
[g-list-item attributes="class:li-class"]Élément 1[/g-list-item]
[g-list-item]Élément 2[/g-list-item]
[g-list-item]Élément 3[/g-list-item]
[/g-list]
```

### Liste avec des icônes

(exemple avec les classes CSS `list-group` de Bootstrap)

<ul class="list-goup">
    <li class="list-group-item"><a href="https://framasphere.org/u/framasoft" ><i class="fa fa-asterisk" aria-hidden="true"></i>&nbsp;Diaspora*</a></li>
    <li class="list-group-item"><a href="https://framapiaf.org/@Framasoft"><i class="fa fa-retweet" aria-hidden="true"></i>&nbsp;Mastodon</a></li>
    <li class="list-group-item"><a href="https://contact.framasoft.org/newsletter"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;Newsletter</a></li>
    <li class="list-group-item"><a href="https://rss.framasoft.org"><i class="fa fa-rss" aria-hidden="true"></i>&nbsp;Flux RSS</a></li>
    <li class="list-group-item"><a href="https://fr.wikipedia.org/wiki/Framasoft"><i class="fa fa-wikipedia-w" aria-hidden="true"></i>&nbsp;Wikipédia</a></li>
</ul>

``` markdown
[g-list attributes="class:list-goup"]
    [g-list-item attributes="class:list-group-item"]
        [g-link url="https://framasphere.org/u/framasoft" icon="asterisk" icon_type="fontawesome"][/g-link]
    [/g-list-item]
    [g-list-item attributes="class:list-group-item"]
        [g-link url="https://framapiaf.org/@Framasoft" icon="retweet" icon_type="fontawesome"][/g-link]
    [/g-list-item]
    [g-list-item attributes="class:list-group-item"]
        [g-link url="https://contact.framasoft.org/newsletter" icon="envelope-o" icon_type="fontawesome"][/g-link]
    [/g-list-item]
    [g-list-item attributes="class:list-group-item"]
        [g-link url="https://rss.framasoft.org" icon="rss" icon_type="fontawesome"][/g-link]
    [/g-list-item]
    [g-list-item attributes="class:list-group-item"]
        [g-link url="https://fr.wikipedia.org/wiki/Framasoft" icon="wikipedia-w" icon_type="fontawesome"][/g-link]
    [/g-list-item]
[/g-list]
```
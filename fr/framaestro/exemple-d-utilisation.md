#Exemple d’utilisation de Framaestro

*Afin de vous présenter un exemple concret, nous avons décidé d’imaginer
la réunion d’un groupe de parole de personnes atteintes d’une addiction,
d’une maladie terrible&nbsp;: celle des gens qui mettent «&nbsp;Frama-&nbsp;» partout
dans leurs phrases. Pour préserver leur anonymat, nous avions décidé de
les appeler «&nbsp;Hioupou&nbsp;», «&nbsp;Yves-Pierre&nbsp;», et «&nbsp;LàPeuple&nbsp;». Merci de votre
compréhension.* 

## Les Framaoliques anonymes se réunissent sur Framaestro.

Hioupou est chargé de préparer la prochaine réunion des
Framaoliques Anonymes. Comme chaque semaine, ce petit groupe se réunit
en ligne pour se soutenir, libérer la parole, et arriver à vivre une vie
sereine, comme tout le monde, une vie où on ne dit pas «&nbsp;Tu peux me
passer la Framagrafeuse&nbsp;?&nbsp;» Cette semaine, au lieu d’utiliser un
*(scrogneugneu-)*Pad pour écrire ensemble le contenu de la réunion, et
un *(non-je-le-dirai-pas-)*Talk pour la visio-conférence, il décide
d’utiliser [Framaestro](https://framaestro.org) *(snif, je l’ai dit)* où
tout peut se trouver au même endroit. Il se rend donc sur le site, et
choisi le nom de son projet "20160112ReunionFA".

![](https://framablog.org/wp-content/uploads/2017/01/Framaestro001-1024x813.png)

*(il a tenté «&nbsp;2016 01 12 Réunion FA&nbsp;» avant de lire que les espaces et
les lettres accentuées ne sont pas admises…)*

Il tombe sur une page blanche, avec une barre d’outils en haut. Certainement la page qui
sera partagée avec ses collègues. Il clique donc sur le bouton «&nbsp;Ajouter
» en haut à droite et décide de commencer par ajouter un Pad à durée
hebdomadaire. 

![](https://framablog.org/wp-content/uploads/2017/01/Framaestro002.png)


En quelques clics, il décide d’afficher aussi sur ce bureau partagé la page
«&nbsp;[Addiction»](https://fr.wikipedia.org/wiki/Addiction) de Wikipédia, une
visio-conférence (avec le bouton <i class="fa fa-video-camera"></i>), ainsi qu’un salon de tchat [par IRC](https://fr.wikipedia.org/wiki/Internet_Relay_Chat) (bouton <i class="fa fa-commenting"></i>) pour les anonymes qui ne veulent pas utiliser la visio conf. Cela ne lui demande pas trop d’efforts&hellip;


&hellip; pour la page Wikipédia, il lui suffit de copier coller l’adresse web (l’URL) dans la barre du menu "ajouter"&nbsp;; 
![wikipedia](https://framablog.org/wp-content/uploads/2017/01/Framaestro003.png "ajout wikipedia")

&hellip; il doit simplement accepter de partager son micro et sa webcam pour la visio-conférence&nbsp;;
![visioconf](https://framablog.org/wp-content/uploads/2017/01/Framaestro004.png "ajout visio-conf")

&hellip; et il lui suffit de rentrer un nom de salon et son pseudonyme pour le salon de tchat IRC.
![chat IRC](https://framablog.org/wp-content/uploads/2017/01/Framaestro005.png "ajout chat IRC")

Après avoir un peu joué à déplacer les cadres et
à les redimensionner (seul le cadre Framatalk lui a donné du fil à
retordre, et en même temps c’est un cadre vidéo ^^), il arrive à un
joli résultat&nbsp;! 

![](https://framablog.org/wp-content/uploads/2017/01/Framaestro006-1024x474.png)

Il ne lui reste plus qu’à partager
son travail avec les autres membres des Framaoliques Anonymes&nbsp;! Il
repère assez vite le bouton de partage <i class="fa fa-share-alt"></i>, et voit qu’il a deux
possibilités&nbsp;: un partage simple avec le permalien (il suffit de
copier/coller le lien du dessus dans un email à Yves-Pierre et
LàPeuple), ou un partage activant certaines options collaboratives.
Aventureux, Hioupou choisit de partager selon les options cochées, et
demande à Framaestro de lui raccourcir le lien&nbsp;: ce sera plus pratique à
faire passer&nbsp;!

![Le menu de partage se personnalise en quelques clics.](https://framablog.org/wp-content/uploads/2017/01/Framaestro007.png "Le menu de partage se personnalise en quelques clics.")

Une joyeuse réunion en ligne plus tard, grâce à Framaestro, les Framaoliques
Anonymes décident de franchir une nouvelle étape dans leur guérison, en
arrêtant collectivement de dire qu’ils trempent leurs Frama-chips dans
du Fraguacamole. Voilà une réunion rondement menée&nbsp;! Bien entendu, ce
n’est là qu’une des utilisations possibles de
[Framaestro](https://framaestro.org)&hellip; à vous d’inventer la vôtre&nbsp;!


![Autre utilisation possible&nbsp;: n’afficher que des sites web&nbsp;!](https://framablog.org/wp-content/uploads/2017/01/slide-framaestro02-1024x350.jpg "Autre utilisation possible&nbsp;: n’afficher que des sites web&nbsp;!")




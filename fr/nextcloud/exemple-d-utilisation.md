# Exemple d’utilisation de Framagenda

## Farida se dégooglise de l’Agenda (et du carnet de contacts)

Farida n’est pas une libriste de la première heure&nbsp;: juste une personne
indépendante à qui ça pose problème de dévoiler sa vie à Google. Agnès,
qui coache l’équipe de football de sa fille, lui a parlé de
[Framagenda](https://framagenda.org)&nbsp;: elle décide de se lancer. Pour
cela elle doit se créer un compte. Mouais, OK, mais que va-t-on faire de
ses données&nbsp;? Elle prend cinq minutes pour lire les [conditions
générales d’utilisation des services
Framasoft](https://framasoft.org/nav/html/cgu.html) (il n’en faut pas
plus) et cela lui convient. Du coup, elle&nbsp;:

1.  se rend donc sur [Framagenda.org](https://framagenda.org) ;
2.  clique sur "S’enregistrer" ;
3.  saisit son adresse email pour recevoir un lien de vérification ;
4.  crée son compte dans la fenêtre ouverte par le lien de vérification.

![framagenda-01](images/framagenda-01.png)

Bien. Une fois son compte créé, elle n’a plus qu’à saisir son mot de
passe, quelque chose de somme toute classique. C’est bien, dès
l’accueil, elle a droit à quelques liens pour savoir comment utiliser
son Framagenda&nbsp;: de la documentation, des outils pour le synchroniser
sur son mobile…

![framagenda-03](images/Framagenda-03.png)

Elle décide de voir si elle arrive à récupérer son agenda personnel
Google. Ce n’est pas hyper intuitif (tiens, Google est moins son ami,
sur ce coup&nbsp;!), mais en suivant [leur
tutoriel](https://support.google.com/calendar/answer/37111?hl=fr), elle
arrive à aller dans les paramètres dudit agenda pour obtenir l’export de
son calendrier.

![framagenda-04](images/framagenda-04.png)

Bon il lui faut le dézipper (merci Google, grrrrr), mais ça y est, elle
a un fichier .ics&nbsp;! Ce doit être ça qu’il lui faut… Dans son Framagenda,
il lui suffit de cliquer sur «&nbsp;paramètres&nbsp;» puis sur «&nbsp;importer un
agenda&nbsp;» pour qu’elle puisse intégrer son Google Agenda à son agenda
personnel (ouf, sauvée, c’est bien le fichier .ics qu’il lui fallait&nbsp;!).

![framagenda-05](images/framagenda-05.png)

La voilà devant une interface d’agenda comme elle en connaît bien, avec
au choix une visualisation de la journée, de la semaine, du mois ; ainsi
qu’un agenda personnel (celui dans lequel elle a importé ses rendez-vous
qui étaient sur Google) et un «&nbsp;Anniversaire de ses contacts&nbsp;» déjà
intégrés.

![framagenda-06](images/framagenda-06.png)

Bon, c’est pas tout ça, mais samedi à 15 h elle a une réunion avec
Agnès, justement, l’entraîneuse de l’équipe de foot de sa fille. Elle
crée donc l’événement en cliquant sur l’horaire. Comme elle veut inviter
Agnès au rendez-vous, elle clique sur «&nbsp;plus&nbsp;» pour détailler cet
événement. Elle rentre l’email d’Agnès, pour que cette dernière soit
prévenue du rendez-vous directement dans sa boite mail.

![framagenda-07](images/framagenda-07.png)

Sandrine trouve que finalement, c’est pas si compliqué que ça, de se
dégoogliser. Elle se dit qu’elle devrait aller rencontrer des libristes
près de chez elle. Du coup, elle va sur l’[Agenda du
Libre](http://agendadulibre.org/), LE site qui regroupe les événements
publics des libristes en France. Farida voit que dans les flux, en bas,
elle peut s’abonner au calendrier des [rencontres libristes de sa
région](http://www.agendadulibre.org/regions).

![framagenda-08](images/framagenda-08.png)

Bon, c’est bien gentil, mais entre le RSS, le WebCal, l’iCal et autres,
elle ne sait que choisir (si ce n’est sa région&nbsp;: l’Occitanie).
Heureusement, lorsqu’elle clique sur "nouvel abonnement" dans son
Framagenda, elle voit qu’on lui demande une adresse Webcal&nbsp;: d’un
clic-droit de la souris, elle copie l’adresse du lien WebCal de l’agenda
du libre, et ajoute cet abonnement à son Framagenda.

![framagenda-09](images/framagenda-09.png)

La voilà désormais avec un agenda bien chargé. C’est bien. Mais ce
serait tout de même mieux si elle pouvait l’avoir sur son téléphone.
Mince&nbsp;: dans l’image qui l’a accueillie lors de son inscription, il y
avait le lien d’un tuto pour synchroniser son agenda avec son téléphone
Android, mais elle a oublié de noter ce lien… Pas de soucis, elle le
retrouve dans l’[aide de Framagenda](Interface-Contacts.html).
Farida télécharge donc [DAVDroid](https://davdroid.bitfire.at/) (3€99…
si ce n’est pas gratuit c’est bien que c’est elle qui soutient le
produit&nbsp;!) et se laisse porter par le tutoriel… Et voilà le travail&nbsp;!

![URL à rentrer/](images/davdroid-1.png "URL à rentrer")

![Synchroniser contacts et calendriers](images/davdroid-2.png "Synchroniser contacts et calendriers")

Oh&nbsp;!
Incroyable&nbsp;! En suivant le tuto d’installation de son agenda sur son
téléphone, elle se rend compte qu’elle peut aussi y prendre les contacts
qu’elle avait confiés à Google (ses ami-e-s, leurs téléphones, leurs
emails et adresses physiques) et les importer dans son Framagenda… Elle
peut même [ajouter les listes de tâches](Interface-Tasks.html) liées à chacun de ses agendas en
utilisant l’application
[OpenTasks](https://play.google.com/store/apps/details?id=org.dmfs.tasks&hl=fr).
Cela ne lui prend que quelques tapotis de plus, alors elle s’exécute
avec plaisir&nbsp;! 

![Listes de taches synchronisées avec Opentasks](images/tasks-android-1.png "Listes de taches synchronisées avec Opentasks")

![Une tâche sur OpenTasks](images/tasks-android-2.png "Une tâche sur OpenTasks")

Du coup, Farida se demande si elle ne peut pas aller
plus loin. Le club de foot de sa fille a besoin d’un agenda partagé pour
afficher les entraînements, matchs et événements des différentes
équipes. Elle tente donc de créer un agenda «&nbsp;FootClub des Arceaux&nbsp;»
avec un événement récurrent (entraînement tous les samedis matin pour
l’équipe de sa fille). Du coup elle va cacher ses autres agendas (en
cliquant sur leurs pastilles colorées) pour en voir le résultat&nbsp;:

![Créer l’événement récurent](images/framagenda-10.png "Créer l’événement récurrent")

Elle partage ensuite la tenue de
cet agenda avec Agnès, la coach. Il lui suffit de cliquer sur l’icône
partager à côté de l’agenda «&nbsp;FootClub des Arceaux&nbsp;» et de rentrer le
pseudonyme d’Agnès. Comme Agnès est aussi sur Framagenda, cela se
complète automatiquement et fonctionne directement.

![Résultat en cachant les autres agendas](images/framagenda-11.png "Résultat en cachant les autres agendas")

Avec cette astuce,
Agnès a elle aussi la main sur cet agenda partagé. Cela lui permet de
rentrer les entraînements des autres équipes et les prochains matchs. En
cherchant à partager le lien public de l’agenda du club de foot, Farida
et elle se rendent compte qu’en regardant les paramètres de cet
affichage public, elles ont justement un code HTML à intégrer dans le
site web du club de foot&nbsp;! Et voilà leur agenda en ligne&nbsp;!

![framagenda-12](images/framagenda-12.png)

Si on intègre ce code ici cela donne&nbsp;:


<iframe src="https://framagenda.org/index.php/apps/calendar/public/XOB8YBABA5955XMW" width="100%" height="300"></iframe>


Ni Farida, ni Agnès ne se définissent comme expertes en informatique ou
même *Geeks*. Pourtant, désormais, leurs rendez-vous, contacts et listes
de tâches n’appartiennent plus ni à Google (pour Farida) ni à Apple
(pour Agnès qui s’est désintoxiquée de l’Iphone). Prochaine étape&nbsp;: voir
avec les libristes du coin si elles peuvent installer le même logiciel
sur les serveurs du Foot-Club des Arceaux et y importer leurs données
Framagenda (on leur a assuré qu’avec Nextcloud, c’est hyper facile).
Cette fois-ci, elles deviendront totalement indépendantes&nbsp;! Si vous
voulez les suivre sur cette route, c’est simple, la voie est libre&nbsp;:
testez [Framagenda](https://framagenda.org)&nbsp;!

# Synchronisation avec Windows Phone
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](../README.md)

Il faut se rendre dans **Paramètres** puis **E-mail+comptes** , **Ajouter un compte** , sélectionnez **iCloud** (qui utilise CalDAV).

Vous renseignez ensuite votre mail et votre mot de passe Framagenda, cliquez sur **Se&nbsp;connecter** et là on vous dit que ça ne fonctionne pas…

Mais si vous vous rendez dans Paramètres avancés et que vous modifiez l’adresse du serveur CalDAV d’iOS par celui de Framagenda&nbsp;: https://framagenda.org/remote.php/dav/, en validant, cela devrait faire fonctionner la synchronisation.

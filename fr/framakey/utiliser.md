# Prise en main

## Qu’est ce qu’il y a dans la Framakey ?

La Framakey est organisée autour de 3 répertoires :

 *  `Framakey`, qui contient tous les éléments de l’interface pour 
    lancer les applications et lire les informations, mais aussi 
    les applications principales de la Framakey, 
    comme par exemple FramaSuite, Synapps ou FramaKiosk.
 *  `Apps`, qui contient les programmes libres embarqués utilisables.
 *  `Data` pour stocker vos données. 

Les évolutions de versions tenant compte de cette architecture, 
nous vous recommandons de ne pas la modifier si vous voulez 
bénéficier des mises à jour de la Framakey. 

## Comment utiliser la Framakey ?

Il faut lancer le programme start.exe situé à la racine de votre clé.
Plusieurs logiciels vont alors se lancer. 
Le « Kiosk » qui est la fenêtre vous permettant de lancer les logiciels 
et contenus de la Framakey, et « aSuite » - représenté par un petit 
logo Framakey situé à côté de l’horloge Windows - qui vous permet un 
accès rapide aux logiciels

## Utilisation du Kiosk

 *  vous lancez les programmes en cliquant sur leur icône,
 *  vous accédez aux informations les laissant votre souris sur leur nom,
 *  le bouton Quitter en haut à droite vous permet de quitter l’interface,
    ce qui n’a aucune conséquence sur les programmes en cours d’activité à ce moment. 

## Utilisation de aSuite

 *  vous cliquez avec le bouton gauche de la souris sur l’icône 
    « grenouille bleue » (« la Framanouille ») pour avoir accès au 
    menu des applications et documents personnels stockés sur la clé,
 *  vous cliquez avec le bouton droit de la souris sur l’icône 
    « grenouille bleue » pour avoir accès au menu des liens Web, mais 
    également à la configuration des menus, au rechargement du fichier 
    de configuration…
 *  vous pouvez aussi coller votre souris tout en haut de votre écran, 
    pour faire apparaître ce menu.
    
## Sur la page d’information

 *  vous avez accès à des articles sur les logiciels utilisables avec la Framakey,
 *  vous pouvez consulter à votre guise les articles de diverses rubriques,
 *  vous pouvez contrôler la page d’information en fermant tous les 
    articles ouverts à un instant, en effectuant une recherche incrémentale 
    sur tous les articles disponibles et, en option, en choisissant de 
    n’afficher qu’un article à la fois et/ou d’activer les animations à l’affichage des articles,
 *  le bouton Accueil en haut à droite vous ramène sur la page d’accueil,
 *  le bouton Quitter en haut à droite vous permet de quitter l’interface,
    ce qui n’a aucune conséquence sur les programmes en cours d’activité à ce moment. 

# Utilisation quotidienne de ma Framakey
## Sauvegardes

Vous avez plusieurs possibilités :

 *  sauvegarde de l’intégralité de la clé : copiez simplement 
    l’intégralité du contenu de la clé sur votre disque dur 
    (vous pouvez utilisez 7-Zip pour gagner de la place). 
    Les applications et leur paramètres seront entièrement sauvegardés. 
    C’est la méthode la plus longue, mais la plus sûre.
 *  sauvegarde complète d’une application : rendez vous dans le 
    dossier `/Apps` de votre clé, et sauvegardez l’application en 
    sélectionnant son répertoire et en le copiant sur votre disque dur.
 *  sauvegarde des paramètres d’une application : rendez vous dans le 
    dossier `/Apps/Nom de l’application` et regardez si le dossier `/Data`
    contient des fichiers. 
    Si oui, vous pouvez alors sauvegarder uniquement les paramètres de 
    l’application en copiant le dossier `Data`, que vous pourrez 
    rétablir par la suite. 
    Si non, procédez à une sauvegarde complète de l’application (cf. ci-dessus).
 *  sauvegarde automatisée : vous pouvez utiliser des outils libres 
    comme Toucan (inclu dans la Framakey) ou Synkron pour automatiser 
    la sauvegarde des dossiers (applications ou `Data`) de votre choix. 

Pour rétablir une sauvegarde, écrasez tout simplement les anciens fichiers par ceux sauvegardés.

## Ajout d’un logiciel

Plusieurs solution sont possibles.

 *  Utiliser Synapps : `Accessoires > Ajout d’applications`, et suivre les instructions
 *  Téléchargez le logiciel souhaité sur Framakey.org, puis dézippez le dans `/Apps`.

Si besoin, voici un [tutoriel détaillé](ajouter-apps.html).

## Mises à jour

Le plus simple reste d’utiliser Synapps qui permet les mises à jour en quelques clics.

Sinon voici la méthode manuelle :

 *  faites une sauvegarde complète de l’application
 *  supprimez le dossier de l’application du répertoire `/Apps` de votre clé
 *  téléchargez la nouvelle version de l’application et dézippez la dans le dossier `/Apps`
 *  si nécessaire, recopiez les données du dossier `/Data` 
    de la sauvegarde réalisée précédemment dans le dossier `/Data` 
    de l’application que vous venez de dézipper. 

Le principe est le même pour la  mise à jour de la Framakey avec le dossier `Framakey`

## J’ai besoin de place : que puis-je supprimer ?

Vous pouvez supprimer le contenu du dossier `/Data` qui contient 
des éléments « Culture Libre » (images, musique, etc) si vous n’y avez pas 
ajouté de documents personnels. Mais cela ne vous fera gagner que 20Mo.
Vous pouvez supprimer des applications du dossier `/Apps` 
Par exemple si vous n’utiliser pas le logiciel Inkscape, supprimer 
le dossier `/Apps/InkscapePortable` vous permettra de gagner près de 200Mo.

Enfin, si vous utilisez une Framakey qui inclu un live USB Linux, 
mais que vous n’utilisez pas les partie Linux vous pouvez supprimer
le dossier `/casper` et le fichier `/casper-rw` (à la racine de la clé) 
qui correspondent à la distribution Linux. Vous économiserez 1900Mo. 

Si vous n’utilisez que très ponctuellement la version Linux embarquée sur la clé, 
vous pouvez aussi redimmensionner la taille du disque persistant `casper-rw` via `Accessoires > Boite à outils`.

## Sécurité de la Framakey

La Framakey intègre une vérification basique de programmes suspects qui 
pourraient être présents à la racine de votre clé (virus, cheval de troie, etc). 
Cependant, il n’existe pas de protection parfaite contre les programmes malveillants.

Voici quelques règles « d’hygiène » pour une bonne utilisation de votre clé :

 *  Utilisez un antivirus fiable sur votre PC et mettez-le à jour régulièrement ;
 *  Scannez régulièrement votre PC et votre clé ;
 *  Si vous branchez votre Framakey sur une machine en laquelle vous 
    n’avez pas toute confiance, celui-ci pourrait infecter 
    les applications de votre clé ;
 *  Si vous utilisez votre clé aprés vous en être servi sur une machine 
    en laquelle vous n’avez pas toute confiance, commencez par rechercher 
    l’éventuelle présence de virus avant de vous en servir 
    (la plupart des antivirus permettent cette opération en faisant un 
    clic-droit sur le lecteur représentant votre clé dans l’explorateur Windows). 
    Ainsi, vous éviterez la contamination d’une autre machine et 
    pourrez éliminer les possibles virus ;
 *  Sauvegardez régulièrement vos données et les applications de votre Framakey. 

## Sécurité des fichiers personnels 

Comment protéger ses fichiers en cas de perte de la clé ?

La Framakey intègre un logiciel libre nommé FreeOTFE qui permet de gérer 
un volume chiffré à l’intérieur duquel vos données seront à l’abri 
(chiffrage AES 256 bits). 
Pour activer ce volume, rendez vous dans `Accessoires > Coffre-fort` 
et suivez les instructions. 

## Usure du matériel

Une clé USB se dégrade (lentement) au fur et à mesure des accès en lecture/écriture.
L’utilisation d’applications portables augmente ces besoins. 
L’usure dépendra donc de la qualité de votre clé.
N’hésitez pas à faire des sauvegardes régulière de votre clé.

Faites aussi attention à bien débrancher la clé USB « proprement ».
Attendez toujours que la diode de votre clé signifiant que des 
opérations de lecture ou d’écriture sont en cours ait cessé de 
clignoter avant de retirer votre clé. 
Ce n’est pas parce que les applications de votre Framakey sont 
apparemment fermées qu’il n’y a plus d’accès. 
Dans la mesure du possible utilisez la procédure de 
déconnexion de périphérique amovible conseillée par Windows. 
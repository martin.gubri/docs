# Liste des raccourcis clavier de Framaslides

## Édition d'éléments et de diapositives

*La touche `Cmd` doit être utilisée à la place de la touche `Ctrl` sur Mac.*

* Copier `Ctrl` + `C`
* Couper `Ctrl` + `X`
* Coller `Ctrl` + `V`
* Supprimer `Suppr`

## Alignement d'élements et de diapositives

* Maintenir la touche `Maj` **après** avoir commencé de déplacer un élément ou une diapositive pour aligner plus simplement les éléments ou les diapositives.

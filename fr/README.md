# Documentations

## Guides

  <div class="col-xs-12">
    <a href="manueldumo/index.html" class="btn btn-lg btn-block btn-default">
      <p><b>Libertés numériques<br>
      <i>— Guide de bonnes pratiques à l'usage des DuMo</i></b></p>
    </a>
  </div>
  <div class="col-xs-12">
    <a href="https://framacloud.org/fr/auto-hebergement/" class="btn btn-lg btn-block btn-default">
      <p><b>L’auto-hébergement facile<br>
      <i>— Puisqu’on n’est jamais mieux servi que par soi-même, créez l’internet que vous voulez</i></b></p>
    </a>
  </div>
    <div class="col-xs-12">
    <a href="comprendre/index.html" class="btn btn-lg btn-block btn-default">
      <p><b>Comprendre le numérique (avec les doigts)<br>
      <i>— Travail en cours...</i></b></p>
    </a>
  </div>

## Services libres de Framasoft

<!--
  La liste est classés par ordre alphabetique de Framaservice

  /!\ Ne pas faire pointer vers la racine du dossier
      mais bien sur "index.html"
-->
<div class="clearfix">
  <div class="col-md-3 col-sm-6">
    <a href="nextcloud/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-calendar"></p>
      <p><b class="violet">Fram</b><b class="vert">agenda</b></p>
      <p><b>Nextcloud</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="wallabag/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-briefcase"></p>
      <p><b class="violet">Frama</b><b class="vert">bag</b></p>
      <p><b>Wallabag</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="zerobin/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-paste"></p>
      <p><b class="violet">Frama</b><b class="vert">bin</b></p>
      <p><b>Zerobin</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="kanboard/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-dashboard"></p>
      <p><b class="violet">Frama</b><b class="vert">board</b></p>
      <p><b>Kanboard</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="umap/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-map"></p>
      <p><b class="violet">Frama</b><b class="vert">carte</b></p>
      <p><b>uMap</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="dolomon/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-hand-pointer-o"></p>
      <p><b class="violet">Frama</b><b class="vert">clic</b></p>
      <p><b>Dolomon</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framadate/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-calendar-check-o"></p>
      <p><b class="violet">Frama</b><b class="vert">date</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="nextcloud/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-cloud-upload"></p>
      <p><b class="violet">Frama</b><b class="vert">drive</b></p>
      <p><b>Nextcloud</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lufi/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-send"></p>
      <p><b class="violet">Frama</b><b class="vert">drop</b></p>
      <p><b>Lufi</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framaforms/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-list-ul"></p>
      <p><b class="violet">Frama</b><b class="vert">forms</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="gitlab/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-git"></p>
      <p><b class="violet">Frama</b><b class="vert">git</b></p>
      <p><b>Gitlab</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lstu/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-link"></p>
      <p><b class="violet">Frama</b><b class="vert">link</b></p>
      <p><b>Lstu</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="sympa/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-group"></p>
      <p><b class="violet">Frama</b><b class="vert">listes</b></p>
      <p><b>Sympa</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framaestro/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-magic"></p>
      <p><b class="violet">Fra</b><b class="vert">maestro</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="turtl/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-sticky-note"></p>
      <p><b class="violet">Frama</b><b class="vert">notes</b></p>
      <p><b>Turtl</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="mastodon/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-mastodon"></p>
      <p><b class="violet">Frama</b><b class="vert">piaf</b></p>
      <p><b>Mastodon</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="lutim/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-photo"></p>
      <p><b class="violet">Frama</b><b class="vert">pic</b></p>
      <p><b>Lutim</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="etherpad/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-align-left"></p>
      <p><b class="violet">Frama</b><b class="vert">pad</b></p>
      <p><b>Etherpad</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="grav/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-globe"></p>
      <p><b class="violet">Frama</b><b class="vert">site</b></p>
      <p><b>Grav</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="strut/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-pie-chart"></p>
      <p><b class="violet">Frama</b><b class="vert">slides</b></p>
      <p><b>Strut</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="diaspora/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-diaspora"></p>
      <p><b class="violet">Frama</b><b class="vert">sphère</b></p>
      <p><b>Diaspora*</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="jitsimeet/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-video-camera"></p>
      <p><b class="violet">Frama</b><b class="vert">talk</b></p>
      <p><b>Jitsi Meet</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="mattermost/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-comments-o"></p>
      <p><b class="violet">Frama</b><b class="vert">team</b></p>
      <b>Mattermost</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="loomio/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-bullhorn"></p>
      <p><b class="violet">Frama</b><b class="vert">vox</b></p>
      <p><b>Loomio</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="shaarli/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-star"></p>
      <p><b class="violet">My</b><b class="vert">Frama</b></p>
      <p><b>Shaarli</b></p>
    </a>
  </div>
</div>


## Culture et logiciels libres

<!--
  La liste est classés par ordre alphabetique de Framaservice

  /!\ Ne pas faire pointer vers la racine du dossier
      mais bien sur "index.html"
-->
<div>
  <div class="col-md-3 col-sm-6">
    <a href="framakey/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-usb"></p>
      <p><b class="violet">Frama</b><b class="bleu">key</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framalibre/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-linux"></p>
      <p><b class="violet">Frama</b><b class="bleu">libre</b></p>
    </a>
  </div>
  <div class="col-md-3 col-sm-6">
    <a href="framapack/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-download"></p>
      <p><b class="violet">Frama</b><b class="bleu">pack</b></p>
    </a>
  </div>
</div>

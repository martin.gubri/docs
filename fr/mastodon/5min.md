## Framapiaf et Mastodon

Rappelons d'abord que Framapiaf est une « instance » du
[logiciel libre](https://fr.wikipedia.org/wiki/Logiciel_libre) [Mastodon](https://framalibre.org/content/mastodon).
Et que Mastodon est un « réseau social libre et fédéré de micro-bloggage ».

Hein ?

Pour résumer, Framapiaf est une alternative à Twitter, avec quelques différences :

  * les utilisateurs de Framapiaf peuvent interagir avec les utilisateurs
    et utilisatrices d'autres instances Mastodon.
    Par exemple, un utilisateur de [Framapiaf.org](https://framapiaf.org)
    peut échanger avec une utilisatrice de [Mamot.fr](https://mamot.fr)
    (**exactement** comme un utilisateur avec un mail @free.fr peut écrire à quelqu'un ayant une adresse @yahoo.com) ;
  * les messages ne sont pas limités à 140 caractères, mais à 500 caractères ;
  * Framapiaf n'affiche pas de publicités : le site est maintenu, grâce
    à [vos dons](https://soutenir.framasoft.org), par
    [l'association Framasoft](https://soutenir.framasoft.org/association),
    dont le but est de promouvoir la culture libre et le logiciel libre ;
  * Framapiaf ne s'intéresse pas à votre vie privée et n'exploite pas
    vos données personnelles ;
  * Framapiaf est géré et modéré par des membres de Framasoft, ce qui a
    des avantages (les modérateurs sont des êtres humains, et bénévoles),
    et des inconvénients (les modérateurs sont des êtres humains, et bénévoles) ;
  * Sur Framapiaf, on ne « tweete » pas, on « pouet », et rien que pour ça,
    ça vaut bien la peine de nous rejoindre !

Au cas où, voici quelques pointeurs qui pourront vous aider à en savoir plus sur ce qu'est (et ce que n'est pas) Mastodon :

  * [« 9 questions pour tout comprendre au  réseau Mastodon »](http://www.numerama.com/tech/246684-debuter-sur-mastodon-9-questions-pour-tout-comprendre-au-reseau-social-decentralise.html) par Numérama ;
  * [« Les CHATONS s’attaquent à l’oiseau Twitter grâce à Mastodon »](https://framablog.org/2017/04/07/les-chatons-sattaquent-a-loiseau-twitter-grace-a-mastodon/) sur le Framablog ;
  * [« Mastodon, qu’est-ce que c’est ? »](https://pixellibre.net/2017/04/mastodon-quest-cest/) par Numendil, sur Pixel Libre.

## Découvrir Framapiaf en 5 minutes

Après la création de votre compte, vous vous retrouvez sur l'écran d'accueil.

![](images/Selection_190_avant.png)

Commencez par affiner vos réglages et préférences en cliquant sur l'icône « Préférences »  en haut à gauche ou sur le lien « Préférences » de la colonne de droite (l'effet sera le même).

![](images/Selection_190.png)

Réglez tout d'abord vos préférences de publication ou de notification. Elles vous permettront notamment de définir si vous souhaitez que vos messages soient, par défaut, publics ou privés.

![](images/Selection_192.png)

Cliquez ensuite sur « Modifier le profil », et renseignez une description, ajoutez une photo à votre profil, etc. Cette étape est bien entendu facultative, mais permettra aux autres utilisateur-ice-s d'en savoir plus sur vous ou vos centres d'intérêts.

![](images/Selection_194.png)

Bon passons maintenant aux choses sérieuses : suivre des utilisateurs !

Cliquez sur « Fil public global ». Cela permet d'afficher les messages publics des inscrits non seulement sur Framapiaf, mais aussi des instances connectées à Framapiaf.
Vous devriez voir apparaître de nombreux messages d'utilisateurs (que vous ne connaissez probablement pas, c'est normal).
Repérez un utilisateur qui poste des messages qui vous intéressent, puis cliquez sur un de ses messages.

![](images/Selection_195.png)

Le détail du message s'affiche alors. Vous pouvez (comme sur Twitter), y répondre, le repartager, le mettre en favori, ou signaler un message/utilisateur malveillant.
Ce qui nous intéresse ici, c'est d'ajouter l'auteur du message à notre liste d'abonnés, c'est-à-dire l'ensemble des personnes dont on souhaite suivre spécifiquement les messages.
On clique à nouveau sur son nom.

![](images/Selection_196.png)

Apparaissent alors plus d'informations sur l'auteur, et surtout une petite icône au-dessus de la photo de son profil.

 ![](images/icone_suivre.png)

 En cliquant dessus, vous ajoutez l'utilisateur à la liste des personnes que vous suivez. Cela signifie que vous verrez apparaître ses messages dans votre fil d'actualités (colonne « Accueil »).

![](images/Selection_198.png)

Voilà :  vous voyez maintenant apparaître les messages de l'utilisateur suivi dans votre fil d'actualités (colonne « Accueil »).

![](images/Selection_200.png)

Vous avez donc maintenant un compte Framapiaf, configuré selon vos souhaits, et vous suivez (au moins) une personne. Il est maintenant temps d'envoyer votre premier message !

Pour cela, rendez-vous dans la première colonne, et saisissez simplement votre message (500 caractères maximum)

![](images/Selection_202.png)

Dernière chose, facultative mais qui peut être utile : personnalisons nos colonnes d'actualités et de notifications.
Pour cela, il suffit de cliquer sur l'icône en haut à droite de ces colonnes, puis d'affiner vos réglages (par exemple pour couper les sons)

![](images/Selection_205.png)

Voilà, vous avez maintenant pris en main les principales fonctionnalités de Framapiaf.

Vous voulez en savoir plus ? Consultez [notre traduction de la documentation officielle](https://docs.framasoft.org/fr/mastodon/)

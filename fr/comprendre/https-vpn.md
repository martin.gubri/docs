# Comprendre la connexion sécurisée et les VPN (avec des suppositoires)


## Imaginons : 

J'ai besoin d'acheter une boite de suppositoires (oui, prenons un exemple qui prouve qu'on a tous des choses à cacher, même débiles / minimes).

## Si je suis en connexion http

Je sors de chez moi, je vais à la pharmacie Bidule (tout le monde dans la rue peut voir que j'y vais).

Je fais ma demande au guichet en plein milieu de la pharmacie. Rien n'empêche une personne mal intentionnée de noter tout ce que je dis au pharmacien (ce que je veux, les instructions du pharmacien, même quelle maladie j'ai si je lâche l'info (et sur internet : récupérer mes informations bancaires).

Je repars chez moi avec ma boite de suppos à la main (classe !) : tout le monde sait que j'ai été à la pharmacie et ce que j'ai acheté.

**Je me balade à poil, on sait tout de moi et en plus on peut dévaliser mon compte en banque.**

## Si je suis en connexion https

Idem, tout le monde sait que je vais à la pharmacie MAIS cette fois c'est la pharmacie Machin, qui propose des guichets fermés.

Je suis seul avec le pharmacien, je fais mon achat et je repars avec la boite de suppos dans un beau petit sac opaque (chiffrement) mais avec quand même le nom de la pharmacie dessus.

**On sait que je reviens de la pharmacie mais personne ne sait ce que j'ai dit ou acheté au pharmacien.**

Il faut que je puisse faire confiance au pharmacien. S'il se trouve qu'il va répéter à qui veut l'entendre ce que j'ai acheté (fuite de données), ma boite de suppos n'est plus un secret (malgré mon magnifique sac opaque).

## Si je me connecte via un VPN

Je ne vais pas à la pharmacie, je vais voir une société de coursiers (tout le monde sait que j'utilise cette société du coup), je suis accueilli dans un bureau fermé, je fais ma demande (chiffrement entre moi et le VPN).

Mon coursier sort, va faire des courses pour plusieurs clients, dont moi, et revient (avec la boite de suppos à la main (http) ou dans un sac (https), selon si je lui ai demandé d'aller à la pharmacie Bidule ou la pharmacie Machin).

Toujours dans un bureau fermé, il met mes suppos dans un sac de sa société et me les donne.

**Je rentre chez moi : on sait juste que je suis allé voir ma société de livraison.**

_Si quelqu'un a pisté le coursier, comme il a fait des courses pour plusieurs personnes en même temps, l'observateur ne peut pas déterminer avec certitude quelle demande correspond à quel client._ Cependant si le coursier conserve un registre de ses courses (conservation des logs), ma visite à la pharmacie peut être connue des personnes ayant accès au registre.

Dans cette affaire de suppos, le coursier **doit être de confiance**. Vous ne confierez pas votre santé dans les mains d'un inconnu, trouvé par petites annonces de « coursiers gratuits », qui peut garder vos notes de courses et remplacer votre aspirine par du viagra ? Dans le cas où la pharmacie Bidule donne la boite de suppos à la main (http) au coursier, ce dernier peut voir ce que ce j'ai commandé et même modifier le médicament (ou ajouter de la sauce piquante sur mes suppos, outch !) sans que je m'en rende compte.

Si je joins ma carte Vitale à ma note de course (sur Internet : s'identifier avec son compte) ou que le pharmacien reconnaisse mon écriture (tracking/pistage web), le pharmacien saura que j'ai fait la commande, même si c'est le coursier qu'il a en face de lui. Il est important de garder en tête qu'utiliser un VPN ne vous rend pas nécessairement anonyme sur Internet.

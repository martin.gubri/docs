Getting started in diaspora\*
=============================

Part 6 – Notifications and conversations
----------------------------------------

The last big feature you should definitely know about is
“conversations.” Before we do that, however, let’s mention
notifications.

### Notifications

Clicking the notifications icon (which looks like a satellite) in the
header bar brings down a drop-down list showing the five most recent
notifications. If you have unread notifications, this will appear as a
number in red over the icon.

![Notify](images/notification-conversation-1.png)

Notifications tend to take the form “Person A has commented on Person
B’s post,” “Person A started sharing with you,” or “Person A has
mentioned you in a post.” From this drop-down, you can:

-   Click the person’s name to view their profile page.
-   Click post to view that post.
-   Click Mark all as read.
-   Click View all to go to the Notifications page.

The notifications page shows you all previous notifications made to you,
25 to a page.

### Conversations

A conversation is a private communication between two or more people. To
access this feature, click the envelope icon in the header bar.

The conversations page consists of two columns: on the left, a list of
conversations you have been involved in; on the right, a view of the
currently selected conversation (if any).

![Conversations page](images/notification-conversation-2.png)

Use the New message button at the top of the left-hand column to start a
new conversation. The button will open a pop-up window in which you can
write a message and add people to include in the conversation.

You can only start conversations with people with whom you have mutual
sharing.

When typing a name into the “to” line, the name will be auto-completed,
just like when you mention someone. Click the name of the person you
want to contact, add a subject in the subject line, type your message
and off you go! It is that simple. You can also use text formatting
here, so feel free to add bold titles and big headers in your message.

In the left-hand column, you’ll see a list of conversations, with the
title, profile picture of the person who started the conversation, the
name of the last person to have replied to it, and to the right the
number of contributions to the conversation (in grey; if there are
unread messages this will be indicated in red), the time since the last
contribution, and underneath, a “multiple persons” icon if there is more
than one person other than you involved. Move your cursor over this icon
to see who else is involved.

You can read a conversation by clicking it in the left-hand column,
after which it will open on the right. At the top left you will find a
list of recipients (including people who haven’t yet contributed to the
conversation). Make sure to check this before starting to gossip! There
may well be more people included in the conversation, all of whom will
see your reply.

Replying to a conversation is pretty straightforward. Once it’s
displayed on your screen, simply type your reply into the publisher
window at the bottom of the conversation, and press the Reply button.

It is possible to delete a conversation from your item list. This can be
done by selecting the conversation in the left-hand menu and then
clicking the x in the top right corner. Blocking a conversation means
you will delete it from the list of conversations and stop receiving any
new replies. Please note that it is still possible for others to send
replies to other participants in the conversation – you will just not
receive them any more.

That is all there is to be said about conversations. You have almost got
through the tutorial! It’s time to read the last part!

[Part 5 – Start
sharing!](5-sharing.html) |
[Part 7 – Finishing
up](7-finishing.html)

#### Useful Resources

-   [Codebase](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Find & report bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - General](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Development](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Discussion -
    General](http://groups.google.com/group/diaspora-discuss)
-   [Discussion -
    Development](http://groups.google.com/group/diaspora-dev)

[![Creative Commons
License](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) is licensed
under a [Creative Commons Attribution 3.0 Unported
License](http://creativecommons.org/licenses/by/3.0/)
